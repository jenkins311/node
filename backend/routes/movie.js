const { request, response } = require('express')
const db = require('../db')
const utils = require('../utils')
const express = require('express')
const router = express.Router()

// Post Movies
router.post('/addMovie', (request, response) => {
    const { movie_title, movie_release_date, movie_time, director_name } = request.body
    
    const query = 'insert into movie ( movie_title, movie_release_date, movie_time, director_name ) values(?,?,?,?)'
    db.pool.execute(query, [movie_title, movie_release_date, movie_time, director_name], (error, result) => {
        response.send(utils.createResult(error, result))
    })
})

router.get('/displayMovie', (request, response) => {
    const query = 'select movie_id, movie_title, movie_release_date, movie_time, director_name from movie'
    db.pool.execute(query, (error, result) => {
        response.send(utils.createResult(error,result))
    })
})

router.put('/updateMovie/:movie_id', (request, response) => {
    const { movie_id } = request.params;
    const { movie_title, movie_release_date, movie_time, director_name } = request.body
    
    const query = 'update movie set movie_title = ?, movie_release_date = ?, movie_time = ?, director_name = ? where movie_id = ?';

    db.pool.execute(query, [movie_title, movie_release_date, movie_time, director_name, movie_id], (error, result) => {
        response.send(utils.createResult(error, result));
    })
})

router.delete("/deleteMovie/:movie_id", (request, response) => {
    const { movie_id } = request.params;
    const query =
      "delete from movie where movie_id = ?";
  
    db.pool.execute(query, [movie_id], (error, result) => {
      response.send(utils.createResult(error, result));
    });
  });

module.exports = router
